let collection = [];
// Write the queue functions below.

function print(){

    return collection;
}

function enqueue(item){
    // add an item

    collection.push(item)
    return collection;
}

function dequeue(item){
    // remove an item
    collection.shift(item)
    return collection;
}

function front(item){
    // get the front item

    let firstItem = collection[0];
    return firstItem;
}

function size(){
    // get the size of the array

    let size = collection.length;
    return size;
}

function isEmpty(array){
    // check array if empty
    
    if(collection.length === 0){
        return true
    }
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};