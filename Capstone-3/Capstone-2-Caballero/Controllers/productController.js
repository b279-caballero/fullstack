const Product = require("../Models/Product");


// Adding Product
module.exports.addProducts = (data) => {
	console.log(data);
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			stocks: data.product.stocks,
			img: data.product.img
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

// Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}


// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}

// Updating product admin only
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		stocks : reqBody.stocks,
		img : reqBody.img
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "Product has been Updated!";
			}
		})
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}

// Archiving product admin only
module.exports.archiveProduct = async (reqParams, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		const product = await Product.findById(reqParams.productId)
		let updateActiveField = {
			isActive : !product.isActive
		};
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

			// Product not archived
			if (error) {
				return false;
			// Product archived successfully
			} else {
				return true;
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};