const User = require("../Models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../Models/Product");

// Registering Users
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number of salt rounds
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		} else {
			return "You are now registered";
		}
	})

};

// Login Users
module.exports.loginUser = (reqBody) => {
	console.log(reqBody.email)
	return User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})

};


// User Authentication
module.exports.userDetail = (data) => {

    return User.findById(data.userId).then(result => {
        	result.password = "";
        	return result
    })

};


// Create Orders
module.exports.createOrder = async (req, res) => {
	 
	if(req.user.isAdmin) {
		res.status(400).send("Admin is not allowed to order")
		return
	}
  	
  let totalAmount = 0;
  
  const products = [];
  const productSaveFunction = []
  
	for(orderIndex in req.body.orders){
		const order = req.body.orders[orderIndex]
		const {
			productId,
			quantity,
		} = order

		const product = await Product.findById(productId)
		totalAmount += product.price * quantity;
		// console.log(product)
		products.push({
    			productId,
      		quantity,
      		productName: product.name,
    	})
    	productSaveFunction.push(product);
	};
  	const newOrder = {
			products,
			totalAmount
		}
    
    const user = await User.findById(req.user.id)
		user.orderedProducts.push(newOrder);
    
    try { 
			await user.save()
			
      products.forEach(async (product, index) => {
      	productSaveFunction[index].stocks -= product.quantity
        await productSaveFunction[index].save()
			})
			res.status(200).send("Purchased Successfully!");
		}catch (error) {
			console.log(error)
			res.status(500).send("Something went wrong! Contact admin");
		}	
  
};


