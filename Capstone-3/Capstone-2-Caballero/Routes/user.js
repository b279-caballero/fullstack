const express = require("express");
const router = express.Router();
const userController = require("../Controllers/user.js");
const auth = require("../auth.js")

//Registering Users Route
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Login Users
router.post("/login", (req, res) => {
	console.log(req)
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// User Authentication
router.post("/detail", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.userDetail({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Create order
router.post("/checkout", auth.verify, userController.createOrder);






module.exports = router;