import LoginCSS from "./Login.css"
import {useNavigate} from "react-router-dom"
import {useEffect} from "react"
import {Link} from "react-router-dom"
import Swal from "sweetalert2"

function Login(){
	const navigate = useNavigate()
	const handleRegister = () => {
		navigate("/register")
	}
	// Function when submitting form
	const handleSubmitForm = (event) => {
		event.preventDefault()
		const form = event.target;
		const formData = new FormData(form)
		const formDataObject = Object.fromEntries(formData)
		const formDataJson = JSON.stringify(formDataObject);
		const loginUrl = "https://furfect-friend-api.onrender.com/users/login"
		fetch(loginUrl,{
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: formDataJson
		}).then(res => res.json()).then(data => {
			localStorage.setItem("token", data.access)
			Swal.fire(
			  'You are logged in!',
			  'success'
			)
			navigate("/");
		})
	}
	useEffect(()=>{
		const tokenStorage = localStorage.getItem("token")
		if(tokenStorage !== null){
			navigate("/");
		}
	},[])
	
	return <>
		<div className="login-container">
			<div className="login-head">
				<h1>Sign in</h1>
				<form className="login-forms" onSubmit={handleSubmitForm}>
					<label for="email" ></label><br/>
		        	<input className="input-email" type="email" placeholder="Email" name="email"/><br/>
		        	<input type="password" placeholder="Password" name="password"/><br/>
		        	<button type="submit">Login</button>
				</form>
			</div>
			<div className="login-body">
				<h1>Hello, Friend!</h1>
				<p>Enter your personal details and start journey with your new fur pet.</p>
				<button className="button-body" onClick={handleRegister}>Sign up</button>	
			</div>
		</div>
	</>
}


export default Login