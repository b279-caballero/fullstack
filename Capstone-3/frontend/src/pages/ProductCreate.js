import ProductCreateCSS from "./ProductCreate.css"

function ProductCreate() {

    const handleCreateProduct = (e) => {
        e.preventDefault();
        const form = e.target;
        const formData = new FormData(form);
        const formDataObject = Object.fromEntries(formData);
        formDataObject.stocks = Number(formDataObject.stocks)
        const formDataJson = JSON.stringify(formDataObject);
        const createProductURL = "https://furfect-friend-api.onrender.com/products"
        fetch(createProductURL,{
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: formDataJson
		}).then(res => res.json()).then(data => {
            alert("Success creation")
		})
    }

    return <>
        <div>
            <form className="display-forms" onSubmit={handleCreateProduct}>
                <label>Name</label><br/>
                <input type="text" name="name" /><br/>
                <label>Image</label><br/>
                <input type="text" name="img" /><br/>
                <label>Description</label><br/>
                <input name="description" /><br/>
                <label>Price</label><br/>
                <input type="text" name="price" /><br/>
                <label>Stocks</label><br/>
                <input type="number" name="stocks" /><br/>
                <button type="submit">Create Product</button>
            </form>
        </div>
    </>
}

export default ProductCreate;