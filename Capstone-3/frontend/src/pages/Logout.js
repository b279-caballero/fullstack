import { useNavigate } from "react-router-dom"
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavbarCSS from '../components/Navbar.css'

function Logout(){
	const navigate = useNavigate()
	const handleLogout = () =>{
		localStorage.removeItem("token")
		navigate("/login")
	}

	return <>
		       <Nav.Link className="navlink" onClick={handleLogout}>Logout</Nav.Link>
	</>
}


export default Logout