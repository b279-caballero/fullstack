import { useEffect, useState } from "react"
import { useParams, useNavigate } from "react-router-dom"
import ProductViewCSS from "./ProductView.css"

function ProductView() {
    const navigate = useNavigate();
    const params = useParams();
    const [product, setProduct] = useState(null);
    const handleBuy = () => {
        localStorage.setItem("toBuy", JSON.stringify(product))
        navigate("/checkout")
    }
    useEffect(() => {
        const retrieveSingleProductURL = `https://furfect-friend-api.onrender.com/products/${params.id}`
        console.log("hello")
        fetch(retrieveSingleProductURL,{
			method: "GET",
			headers: {
				"Content-Type" : "application/json",
			},
		}).then(res => res.json()).then(data => {
            setProduct(data)
		})
    }, [params.id])

    return <>
    
    {   
        product ?
        <div className="product-single"> 
            <h1>{product.name}</h1>
            <img className="product-single-img" src={product.img}/>
            <p className="description-single">{product.description}</p>
            <p>&#8369; {product.price}</p>
            <p>Stocks: {product.stocks}</p>
            <button onClick={handleBuy}>Buy</button>
        </div>
         
         : <h1>Loading...</h1>
    }
         
    

    </>
}

export default ProductView;