import RegisterCSS from "./Register.css"
import {useNavigate} from "react-router-dom"
import Swal from "sweetalert2"

function Register(){
	// function when submitting
	const navigate = useNavigate()
	const handleSubmitForm=(event) => {
		// stops all default action from event(submit event)
		event.preventDefault()
		const form = event.target;
		const formData = new FormData(form)
		const formDataObject = Object.fromEntries(formData)
		if(formDataObject.password !== formDataObject.password1){
			alert("Password not matched")
			return
		}
		delete formDataObject.password1
		const formDataJson = JSON.stringify(formDataObject);
		const registerUrl = "https://furfect-friend-api.onrender.com/users/register"
		fetch(registerUrl,{
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NWQ3NzNlZjAwMmI3ZmMyM2E1OTM2NyIsImVtYWlsIjoibWFubnlAbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjgzODUyNDU1fQ.kH85TDyQ7hUPskn_etucbgBuW9M5shfNQ9Ns5SnI3TU"
			},
			body: formDataJson
		}).then(res => res.text()).then(data =>{
			Swal.fire('You are registered')
			navigate("/login");
		})
	}


	return <>
      <div className="form-text">
    		<h1>Register Here</h1>
        	<p>Man's furever friend.🐾</p>
        	<form onSubmit={handleSubmitForm}>
        			<input className="input-register" id="firstname" placeholder="First Name" type="text" name="firstName"/><br/>
        			<input className="input-register" type="text" placeholder="Last Name" name="lastName"/><br/>
        			<input className="input-register" type="text" placeholder="Mobile Number" name="mobileNo"/><br/>
        			<input className="input-register" type="email" placeholder="Email Address" name="email"/><br/>
        			<input className="input-register" type="password" placeholder="Password" name="password"/><br/>
					<input className="input-register" type="password" placeholder="Confirm Password" name="password1"/><br/>
              <button className="regbot" type="submit">Register</button>
        	</form>
        <p>Already have an account? <a href={"/login"}>Click Here</a> to log in</p>
      </div>
    </>	
}

export default Register;