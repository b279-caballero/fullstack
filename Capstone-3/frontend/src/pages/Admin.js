import Sidebar from "../components/Sidebar"
import SidebarCSS from "../components/Sidebar.css"
import { Outlet } from "react-router-dom";
import "./Admin.css"

function AdminDashboard(){
	return( 
		<div className="Admin-App">
			<Sidebar />
			<div className="admin-content">
				<Outlet />
			</div>
		</div>
	);	
}


export default AdminDashboard