import { useEffect, useState } from "react";
import {Link} from "react-router-dom";
import ProductAdminCSS from "./ProductAdmin.css"

function ProductAdmin(){
    const [products, setProducts] = useState([]);

    useEffect(() => {

        const getProductsURL = "https://furfect-friend-api.onrender.com/products/all"
        fetch(getProductsURL,{
			method: "GET",
			headers: {
				"Content-Type" : "application/json",
			},
		}).then(res => res.json()).then(data => {
            setProducts(data)
		})
    }, [])

    // return <Link to={"/create"}>+</Link>
    return <div className="table-product">
        
        
        <table>
            <thead>
            <th>Name</th>
            <th>Description</th>
            <th>Price (Php)</th>
            <th>Stocks</th>
            </thead>
            <tbody>
                {products.map(product => 
                    <tr>
                        <td>{product.name}</td>
                        <td>{product.description}</td>
                        <td>&#8369; {product.price}</td>
                        <td>{product.stocks}</td>
                        <td><Link to={`/admin/products/update/${product._id}`}>Update</Link></td>
                    </tr>    
                )}
            </tbody>


        </table>
    </div>

}

export default ProductAdmin;