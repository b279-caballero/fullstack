import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ProductUpdateCSS from "./ProductUpdate.css"

function ProductUpdate() {
    const params = useParams();
    const [product, setProduct] = useState(null)
    const handleUpdateProduct = (e) => {
        e.preventDefault();
        const form = e.target;
        const formData = new FormData(form);
        const formDataObject = Object.fromEntries(formData);
        formDataObject.stocks = Number(formDataObject.stocks)
        const formDataJson = JSON.stringify(formDataObject);
        const updateProductURL = `https://furfect-friend-api.onrender.com/products/${params.id}`
        fetch(updateProductURL,{
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: formDataJson
		}).then(res => res.text()).then(data => {
            alert("Updated successfully")
		})
    }

    const handleArchive = () => {
        const archiveProductURL = `https://furfect-friend-api.onrender.com/products/${params.id}/archive`
        fetch(archiveProductURL,{
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
			},
		}).then(res => res.text()).then(data => {
            setProduct(state => {
                const newState = {...state};
                newState.isActive = !state.isActive
                return newState
            })
           
		})
    }

    useEffect(() => {
        const retrieveProductURL = `https://furfect-friend-api.onrender.com/products/${params.id}`
        console.log("hello")
        fetch(retrieveProductURL,{
			method: "GET",
			headers: {
				"Content-Type" : "application/json",
			},
		}).then(res => res.json()).then(data => {
            setProduct(data)
		})
    }, [params.id])

     
    return product ? <>   
        <form className="update-forms" onSubmit={handleUpdateProduct}>
            <label>Name</label><br/>
            <input type="text" name="name" defaultValue={product.name} /><br/>
            <label>Image</label><br/>
            <input type="text" name="img" defaultValue={product.img} /><br/>
            <label>Description</label><br/>
            <input name="description" defaultValue={product.description} /><br/>
            <label>Price</label><br/>
            <input type="text" name="price" defaultValue={product.price} /><br/>
            <label>Stocks</label><br/>
            <input type="number" name="stocks" defaultValue={product.stocks} /><br/>
            <button type="submit">Update</button>
        </form>

        {product.isActive ?
            <div></div>
            :
            <div></div>
        }
        <button className="archive-button" onClick={handleArchive}>{product.isActive ? "ARCHIVE" : "UNARCHIVE"}</button>
    </> : <div>Loading....</div>
}

export default ProductUpdate;