import { useNavigate } from "react-router-dom"
import HomeCSS from "./Home.css"


function Home(){
	const navigate = useNavigate()
	const handleProduct = () =>{
		navigate("/products")
	}

	return <>
		<div  className="home-head">
			<div className="home-body">
				<h3 className="p">FUR'FECT FRIEND</h3>
				<p className="p">Unleash the joy of companionship. Welcome to our pet store, where wagging tails and purring hearts find their forever homes.</p>
				<p className="p">Every dog deserves a home, but not every home deserves a dog.</p>
				<button onClick={handleProduct}>View All</button>
			</div>
		</div>
	</>	
}


export default Home