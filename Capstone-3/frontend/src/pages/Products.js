import { useEffect, useState } from "react";
import "./Products.css"
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

function Products(){
    const [products, setProducts] = useState([]);
    useEffect(() => {
        const getProductsURL = "https://furfect-friend-api.onrender.com/products"
        fetch(getProductsURL,{
			method: "GET",
			headers: {
				"Content-Type" : "application/json",
			},
		}).then(res => res.json()).then(data => {
            setProducts(data)
		})
    }, [])

	return <>
	<div className="product-whole">
		<h1 className="h1-product">Pick your friend</h1>
		<hr/>
		<div className="container">
			{products.map(product =>
				<Card key={product._id} className="card-head">
					<Card.Img className="card-image" variant="top" src={product.img} />
					<Card.Body className="card-body">
							<Card.Title>{product.name}</Card.Title>
							<Card.Subtitle>&#8369; {product.price}</Card.Subtitle>
								<Card.Text>
								{product.description}
								</Card.Text>	
								<Link to={`/products/${product._id}`} variant="primary">Details</Link>
					</Card.Body>	
				</Card>
			)}
		</div>
	</div>
	</>
}


export default Products