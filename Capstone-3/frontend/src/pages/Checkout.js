import { useState } from "react"
import { useNavigate } from "react-router-dom"
import CheckoutCSS from "./Checkout.css"
import Swal from "sweetalert2"


function Checkout () {
   
    const navigate = useNavigate()
    const [quantity, setQuantity] = useState(1)
    const toBuy = JSON.parse(localStorage.getItem("toBuy"))
    const handleQuantityChange = (e) => {
        const inputValue = e.target.value
        if (inputValue <= toBuy.stocks)
            setQuantity(inputValue)
        else
            alert("Quantity is beyond stocks")
    }
    const handleCompleteOrder = () => {
        const checkoutURL = "https://furfect-friend-api.onrender.com/users/checkout"
        const order = {
            "orders": [
              {
                "productId": toBuy._id,
                "quantity": quantity
              },
            ]
          }
        fetch(checkoutURL,{
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify(order)
		}).then(res => res.text()).then(data => {
            navigate("/checkout/complete")
		})
    }
    
    
    return <div className="checkout-product">
            <img src={toBuy.img}/>
            <h1>{toBuy.name}</h1>
            <p>&#8369; {toBuy.price}</p>
            <input type="number" value={quantity} onChange={handleQuantityChange}></input><br/>
            <button onClick={handleCompleteOrder}>Complete Order</button>
        </div>
}

export default Checkout ;