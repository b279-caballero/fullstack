import CompleteOrderCSS from "./CompleteOrder.css"
import {useNavigate} from "react-router-dom"

function CompleteOrder () {
const navigate = useNavigate()
    const handleCompleteOrder = () => {
        navigate("/")
    }

    return <>
        <div className="complete-order">
            <img src="/images/complete.png"/>
                <p>Order Completed !</p>
                <button onClick={handleCompleteOrder}>Home</button>
        </div>
    </>
}

export default CompleteOrder ;