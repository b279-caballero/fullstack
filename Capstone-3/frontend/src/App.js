import logo from './logo.svg';
import './App.css';
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout"
import Products from "./pages/Products"
import Admin from "./pages/Admin"
import Layout from "./components/Layout";
import ProductAdmin from './pages/ProductAdmin';
import ProductCreate from './pages/ProductCreate';
import ProductUpdate from './pages/ProductUpdate';
import ProductView from './pages/ProductView';
import Checkout from './pages/Checkout';
import CompleteOrder from './pages/CompleteOrder';

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <Home />
      },
      {
        path: "register",
        element: <Register />
      },
      {
        path: "login",
        element: <Login />
      },
      {
        path: "logout",
        element: <Logout />
      },
      {
        path: "products",
        element: <Products />
      },
      {
        path: "products/:id",
        element: <ProductView />
      },
      {
        path: "checkout",
        element: <Checkout />
      },
      {
        path: "checkout/complete",
        element: <CompleteOrder />
      }
    ]
  },
  {
    path: "/admin",
    element: <Admin />,
    children: [
      {
        path: "/admin/products",
        element: <ProductAdmin />
      },
      {
        path: "/admin/products/create",
        element: <ProductCreate />
      },
      {
        path: "/admin/products/update/:id",
        element: <ProductUpdate />
      }
    ]
  }
]);



function App() {
   return <RouterProvider router={router} />

}

export default App;
