import {useEffect, useState} from "react"
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavbarCSS from './Navbar.css'
import { Link, NavLink } from "react-router-dom";
import Logout from "../pages/Logout"
import { matchRoutes, useLocation } from "react-router-dom"

function AppNavbar(){
	const [ isLoggedOut, setIsLoggedOut] = useState(true);
	const location = useLocation()
	useEffect(() => {
		if(localStorage.getItem("token")) 
			setIsLoggedOut(false)
		else
			setIsLoggedOut(true)
	}, [location])

	return   <>
			 <Navbar className="navbar-main">
		          <Navbar.Brand as={NavLink} to={"/"} className="navbar-brand">FUR'FECT FRIEND</Navbar.Brand>
		          <Nav>
		            
		            <Nav.Link as={NavLink} to={"/products"} className="navlink">Products</Nav.Link>
		            
		            
					{ (!isLoggedOut) ?
					<>  <Logout />  </>  :<> <Nav.Link as={NavLink} to={"/login" } className="navlink">Login</Nav.Link> <Nav.Link as={NavLink} to={"/register"} className="navlink">Register</Nav.Link> </>
					}
		          </Nav>
		      </Navbar>
		  	</>
}


export default AppNavbar