import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import InstagramIcon from '@mui/icons-material/Instagram';
import CopyrightIcon from '@mui/icons-material/Copyright';
import FooterCSS from "./Footer.css"


function Footer(){
	return <>
		<div className="footer-main">
			<div className="footer-head">
				<span><FacebookIcon /></span>
				<span><LinkedInIcon /></span>
				<span><InstagramIcon /></span>
			</div>
			<div className="footer-foot">	
				<p>Personal Projects:</p>
				<h4>Arjay Ceazar S. Caballero</h4>
				<p>Copyright 2023. Allrights reserved.</p>
			</div>
		</div>
	</>
}


export default Footer