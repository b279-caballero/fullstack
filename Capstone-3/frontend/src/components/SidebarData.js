import React from "react"
import HomeIcon from '@mui/icons-material/Home';
import PetsIcon from '@mui/icons-material/Pets';
import AddIcon from '@mui/icons-material/Add';
import InventoryIcon from '@mui/icons-material/Inventory';

export const SidebarData = [
    {
        title: "Home",
        icon: <HomeIcon />,
        link: "/"
    },
    {
        title: "Get All Products",
        icon: <PetsIcon />,
        link: "/admin/products"
    },
    {
        title: "Create Product",
        icon: <AddIcon />,
        link: "/admin/products/create"
    },
    {
        title: "Products",
        icon: <InventoryIcon/>,
        link: "/products"
    }
]

